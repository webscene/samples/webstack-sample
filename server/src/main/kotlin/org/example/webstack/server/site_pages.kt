package org.example.webstack.server

import java.io.InputStream
import org.webscene.core.template.PageTemplate
import org.webscene.core.html.HtmlCreator as html

// Provides the web pages for the web app.

private val scriptSources = fetchScriptSources()

fun createHomePage(): String {
    val template = PageTemplate(title = "Kotlin Web Stack", pageId = "home")

    scriptSources.forEach { template.scripts.add(createJSScript(it)) }
    return template.content?.createText() ?: ""
}

fun createWelcomePage(): String {
    val template = PageTemplate(title = "Kotlin Web Stack - Welcome", pageId = "welcome")

    scriptSources.forEach { template.scripts.add(createJSScript(it)) }
    return template.content?.createText() ?: ""
}

private fun createJSScript(src: String) = html.parentElement("script") {
    attributes["src"] = src
    attributes["type"] = "application/javascript"
}

private fun fetchScriptSources(): Array<String> {
    val inputStream: InputStream? = Server::class.java.getResourceAsStream("/config/scripts.txt")
    val tmp = mutableListOf<String>()

    inputStream?.use { input ->
        input.bufferedReader().forEachLine { line ->
            if (line != "\n") tmp += line
        }
    }
    return tmp.toTypedArray()
}
