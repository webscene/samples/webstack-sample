@file:Suppress("PropertyName")

import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

buildscript {
    repositories {
        jcenter()
        mavenCentral()
    }

    dependencies {
        classpath(Dependency.shadowPlugin)
    }
}

plugins {
    kotlin("jvm") version Version.kotlin
    application
}

application.mainClassName = ServerConfig.mainClass

repositories {
    maven { url = uri(localRepoDir) }
}

apply {
    plugin(PluginId.shadow)
}

dependencies {
    compile(kotlin(module = "stdlib-jdk8", version = Version.kotlin))
    compile(Dependency.vertxCore)
    compile(Dependency.vertxWeb)
    compile(Dependency.vertxKotlin)
    compile(Dependency.websceneCoreJvm)
}

val compileKotlin by tasks.getting(KotlinCompile::class) {
    kotlinOptions.jvmTarget = "1.8"
}
val run by tasks.getting(JavaExec::class) {
    val watcherPath = "src/**/*"
    val watcherAction = "${projectDir.parent}/gradlew classes"

    args(
        "run",
        ServerConfig.mainVerticle,
        "--redeploy=$watcherPath",
        "--launcher-class=${ServerConfig.mainClass}",
        "--on-redeploy=$watcherAction"
    )
}
val shadowJar by tasks.getting(ShadowJar::class) {
    baseName = "webstack"
    classifier = ""
    append("src/main/resources/public")
    manifest {
        attributes["Main-Verticle"] = ServerConfig.mainVerticle
    }
}
