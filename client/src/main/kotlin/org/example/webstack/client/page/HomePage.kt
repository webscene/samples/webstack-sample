package org.example.webstack.client.page

import org.example.webstack.client.HttpStatus
import org.w3c.dom.Element
import org.w3c.xhr.XMLHttpRequest
import org.webscene.core.HttpMethod
import org.webscene.core.dom.DomEditor
import org.webscene.core.dom.DomQuery
import org.webscene.core.dom.textBoxValue
import org.webscene.core.html.HtmlTag
import org.webscene.core.html.ParentHtmlTag
import org.webscene.core.html.element.ParentHtmlElement
import org.webscene.core.html.element.input.InputType
import org.webscene.core.html.element.toDomElement
import org.webscene.core.httpClient
import org.webscene.core.html.HtmlCreator as html
import org.webscene.core.html.HtmlInputCreator as htmlInput

class HomePage {
    private val mainHeader = html.heading {
        id = "main-header"
        +"Home Page"
    }
    private val mainLayout = html.div {
        id = "main-layout"
        attributes["container"] = "true"
        children += mainHeader
        children.addAll(createContent())
    }

    init {
        DomEditor.editSection(domElement = mainLayout.toDomElement())
        DomEditor.replaceElement {
            mainHeader.attributes["size"] = "large"
            mainHeader.txtContent = "Web stack home page."
            mainHeader
        }
        DomQuery.elementById<Element>("submit-btn").addEventListener(type = "click", callback = {
            doHttpRequest(mainLayout)
        })
    }

    private fun createContent(): Array<HtmlTag> {
        val nameTxt = htmlInput.input(type = InputType.TEXT, autoFocus = true) {
            id = "name-txt"
            attributes["placeholder"] = "Enter name"
        }
        val submitBtn = htmlInput.button {
            id = "submit-btn"
            +"Submit"
        }
        val msg = html.bold {
            id = "msg"
            attributes["visible"] = "false"
        }

        return arrayOf(nameTxt, submitBtn, msg)
    }

    private fun doHttpRequest(mainLayout: ParentHtmlTag) {
        val name = DomQuery.elementById<Element>("name-txt").textBoxValue
        val reqData = arrayOf<Pair<String, *>>("name" to name)

        httpClient(url = "/api/greeting", method = HttpMethod.POST, reqData = reqData, sendNow = true) {
            onload = {
                if (status == HttpStatus.OK.num) processHttpResponse(this, mainLayout)
            }
        }
    }

    private fun processHttpResponse(client: XMLHttpRequest, mainLayout: ParentHtmlTag) = DomEditor.replaceElement {
        (mainLayout.children.last() as ParentHtmlElement).apply {
            attributes["visible"] = "true"
            +client.responseText
        }
    }
}
